package co.atlvntis.engine.event;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Map;

public interface Listener {


    default Method[] getListenerMethods() {

        Method[] classMethods = this.getClass().getMethods();
        Method[] methods = new Method[classMethods.length];

        int index = 0;
        for (Method method : classMethods) {

            if(!method.isAnnotationPresent(Handler.class)) continue;

            Parameter[] parameters = method.getParameters();
            if(parameters.length != 1) continue;
            Parameter eventParameter = parameters[0];
            if(eventParameter.getType().getSuperclass() != Event.class) continue;

            System.out.println("Method found for event " + method.getParameters()[0].getType().getSimpleName() + " in class " + this.getClass().getSimpleName());

            methods[index] = method;
            index++;

        }

        return methods;

    }


}
