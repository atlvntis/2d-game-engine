package co.atlvntis.engine.event.list;

import co.atlvntis.engine.event.Event;

import java.util.concurrent.atomic.AtomicInteger;

public class GameUpdateEvent extends Event {

    private AtomicInteger frames;

    public GameUpdateEvent(int frames) {
        this.frames = new AtomicInteger(frames);
    }

    public void setFrames(int frames) {
        this.frames.set(frames);
    }

    public int getFrames() {
        return frames.get();
    }
}
