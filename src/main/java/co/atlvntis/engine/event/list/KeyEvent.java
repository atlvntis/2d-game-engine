package co.atlvntis.engine.event.list;

import co.atlvntis.engine.event.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class KeyEvent extends Event {

    private final int keyCode;

}
