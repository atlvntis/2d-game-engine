package co.atlvntis.engine.event.list;

import co.atlvntis.engine.event.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class KeyUpEvent extends KeyEvent {

    public KeyUpEvent(int keyCode) {
        super(keyCode);
    }
}
