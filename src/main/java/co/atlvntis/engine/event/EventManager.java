package co.atlvntis.engine.event;

import lombok.SneakyThrows;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class EventManager {

    private Map<Class<? extends Event>, Map<Object, Method>> handlers;

    public EventManager() {
        this.handlers = new ConcurrentHashMap<>();
    }

    public void registerListener(Listener eventListener) {

        Method[] methods = eventListener.getListenerMethods();

        for (Method method : methods) {

            if(method == null) continue;
            Class<? extends Event> event = (Class<? extends Event>) method.getParameters()[0].getType();

            if(handlers.containsKey(event)) {
                handlers.get(event).put(eventListener, method);
            } else {
                Map<Object, Method> map = new HashMap<>();
                map.put(eventListener, method);
                handlers.put(event, map);
            }

        }

    }

    @SneakyThrows
    public void callEvent(Event event) {

        if(handlers.containsKey(event.getClass())) {
            for (Map.Entry<Object, Method> entry : handlers.get(event.getClass()).entrySet()) {
                entry.getValue().invoke(entry.getKey(), event);
            }
        }

    }

}
