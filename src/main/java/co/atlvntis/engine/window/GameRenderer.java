package co.atlvntis.engine.window;

import co.atlvntis.engine.event.Handler;
import co.atlvntis.engine.event.Listener;
import co.atlvntis.engine.event.list.GameUpdateEvent;

import java.awt.image.DataBufferInt;

public class GameRenderer implements Listener {

    private int height;
    private int width;
    private int[] pixels;

    public GameRenderer(final GameWindow gameWindow) {

        GameDimensions gameDimensions = gameWindow.getGameDimensions();

        this.height = gameDimensions.getHeight();
        this.width = gameDimensions.getWidth();

        this.pixels = ((DataBufferInt)gameWindow.getBufferedImage().getRaster().getDataBuffer()).getData();

    }

    void clear() {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] += i*2;
        }
    }

    @Handler
    public void onUpdate(GameUpdateEvent event) {

        clear();

    }

}
