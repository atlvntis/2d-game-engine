package co.atlvntis.engine.window;

import lombok.Getter;

@Getter
public class GameDimensions {

    private int height;
    private int width;
    private double scale;

    private String title;

    public GameDimensions() {

        this.height = 320;
        this.width = 240;
        this.scale = 1D;

        this.title = "My window";

    }

    public GameDimensions withHeight(int height) {
        this.height = height;
        return this;
    }

    public GameDimensions withWidth(int width) {
        this.width = width;
        return this;
    }

    public GameDimensions withScale(double scale) {
        this.scale = scale;
        return this;
    }

    public GameDimensions withTitle(String title) {
        this.title = title;
        return this;
    }

    public static GameDimensions DEFAULT() {
        return new GameDimensions().withScale(4);
    }


}
