package co.atlvntis.engine.window;

import co.atlvntis.engine.event.Handler;
import co.atlvntis.engine.event.Listener;
import co.atlvntis.engine.event.list.GameUpdateEvent;
import lombok.Getter;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

@Getter
public class GameWindow implements Listener {

    private final GameDimensions gameDimensions;

    private JFrame jFrame;
    private BufferedImage bufferedImage;
    private Canvas canvas;
    private BufferStrategy bufferStrategy;
    private Graphics graphics;

    public GameWindow(final GameDimensions gameDimensions) {

        this.gameDimensions = gameDimensions;

        this.bufferedImage = new BufferedImage(gameDimensions.getWidth(), gameDimensions.getHeight(), BufferedImage.TYPE_INT_RGB);

        int scaledWidth = (int) (gameDimensions.getWidth() * gameDimensions.getScale());
        int scaledHeight = (int) (gameDimensions.getHeight() * gameDimensions.getScale());

        Dimension dimension = new Dimension(scaledWidth, scaledHeight);

        this.canvas = new Canvas();
        this.canvas.setPreferredSize(dimension);
        this.canvas.setMaximumSize(dimension);
        this.canvas.setMinimumSize(dimension);

        this.jFrame = new JFrame(gameDimensions.getTitle());
        this.jFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.jFrame.setLayout(new BorderLayout());
        this.jFrame.add(canvas, BorderLayout.CENTER);
        this.jFrame.pack();
        this.jFrame.setLocationRelativeTo(null);
        this.jFrame.setVisible(true);
        this.jFrame.setResizable(false);

        this.canvas.createBufferStrategy(2);
        this.bufferStrategy = this.canvas.getBufferStrategy();
        this.graphics = this.bufferStrategy.getDrawGraphics();

    }

    @Handler
    public void onUpdate(GameUpdateEvent event) {

        this.graphics.drawImage(
                bufferedImage,
                0, 0,
                canvas.getWidth(), canvas.getHeight(),
                null);

        this.bufferStrategy.show();

    }

}
