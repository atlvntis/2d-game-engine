package co.atlvntis.engine;

import co.atlvntis.engine.event.EventManager;
import co.atlvntis.engine.event.Listener;
import co.atlvntis.engine.input.GameInputs;
import co.atlvntis.engine.thread.GameThread;
import co.atlvntis.engine.util.Lazy;
import co.atlvntis.engine.window.GameDimensions;
import co.atlvntis.engine.window.GameRenderer;
import co.atlvntis.engine.window.GameWindow;
import lombok.Getter;

@Getter
public class GameContainer {

    private static final Lazy<GameContainer> instance = new Lazy<>();

    private final GameThread gameThread;

    private GameWindow gameWindow;
    private GameRenderer gameRenderer;
    private GameInputs gameInputs;

    @Getter
    private EventManager eventManager;

    public GameContainer() {

        instance.compute(() -> (this));

        this.gameThread = new GameThread(this);

        this.gameWindow = new GameWindow(GameDimensions.DEFAULT());
        this.gameRenderer = new GameRenderer(gameWindow);
        this.gameInputs = new GameInputs(this);

        this.eventManager = new EventManager();
        this.eventManager.registerListener(gameRenderer);
        this.eventManager.registerListener(gameWindow);
        this.eventManager.registerListener(gameInputs);

    }

    public final GameContainer withListener(Listener listener) {
        this.eventManager.registerListener(listener);
        return this;
    }

    public final GameContainer start() {
        gameThread.start();
        return this;
    }

    public static GameContainer getInstance() {
        return instance.getValue();
    }

}
