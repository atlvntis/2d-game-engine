package co.atlvntis.engine.thread;

import co.atlvntis.engine.GameContainer;
import co.atlvntis.engine.event.list.GameUpdateEvent;
import lombok.SneakyThrows;

import java.util.*;

public class GameThread implements Runnable {

    private static final double UPDATE_CAP = 1.0/60.0;

    private final GameContainer gameContainer;
    private final Thread thread;

    private final GameUpdateEvent gameUpdateEvent;

    private boolean running = false;

    public GameThread(final GameContainer gameContainer) {

        this.gameContainer = gameContainer;
        this.thread = new Thread(this);

        this.gameUpdateEvent = new GameUpdateEvent(0);
    }

    public final void start() {

        this.thread.run();

    }

    @SneakyThrows
    @Override
    public void run() {

        double firstTime;
        double lastTime = System.nanoTime() / 1000000000.0;
        double passedTime;
        double unprocessedTime = 0D;

        boolean render = false;

        double frameTime = 0D;
        int frames = 0;
        int framesPerSecond = 0;

        running = true;

        while(running) {

            render = false;

            firstTime = System.nanoTime() / 1000000000.0;
            passedTime = firstTime - lastTime;
            lastTime = firstTime;

            unprocessedTime += passedTime;
            frameTime += passedTime;

            while(unprocessedTime >= UPDATE_CAP) {

                unprocessedTime -= UPDATE_CAP;
                render = true;

                if(frameTime >= 1D) {
                    frameTime = 0;
                    framesPerSecond = frames;
                    frames = 0;
                }

            }

            if(render) {

                this.gameUpdateEvent.setFrames(frames);
                this.gameContainer.getEventManager().callEvent(this.gameUpdateEvent);
                frames++;

            } else {
                Thread.sleep(1);
            }

        }

    }


}
