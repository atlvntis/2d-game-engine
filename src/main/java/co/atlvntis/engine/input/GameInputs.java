package co.atlvntis.engine.input;

import co.atlvntis.engine.GameContainer;
import co.atlvntis.engine.event.Handler;
import co.atlvntis.engine.event.Listener;
import co.atlvntis.engine.event.list.GameUpdateEvent;
import co.atlvntis.engine.event.list.KeyDownEvent;
import co.atlvntis.engine.event.list.KeyHeldEvent;
import co.atlvntis.engine.event.list.KeyUpEvent;

import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;

public class GameInputs implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener, Listener {

    private static final int KEY_COUNT = 256;
    private static final int BUTTON_COUNT = 5;

    private static final double SCALE = GameContainer.getInstance().getGameWindow().getGameDimensions().getScale();

    private final GameContainer gameContainer;

    private boolean[] keys = new boolean[KEY_COUNT];
    private boolean[] keysLast = new boolean[KEY_COUNT];

    private boolean[] buttons = new boolean[BUTTON_COUNT];
    private boolean[] buttonsLast = new boolean[BUTTON_COUNT];

    private int mouseX, mouseY;
    private int scroll;

    public GameInputs(final GameContainer gameContainer) {

        this.gameContainer = gameContainer;

        Canvas canvas = gameContainer.getGameWindow().getCanvas();
        canvas.addKeyListener(this);
        canvas.addMouseListener(this);
        canvas.addMouseMotionListener(this);
        canvas.addMouseWheelListener(this);

    }

    @Handler
    public void onGameUpdate(GameUpdateEvent event) {

        System.arraycopy(keys, 0, keysLast, 0, KEY_COUNT);
        System.arraycopy(buttons, 0, buttonsLast, 0, BUTTON_COUNT);

        scroll = 0;

    }


    @Override
    public void keyTyped(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        KeyHeldEvent event = new KeyHeldEvent(keyCode);
        gameContainer.getEventManager().callEvent(event);
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        keys[keyCode] = true;
        KeyDownEvent event = new KeyDownEvent(keyCode);
        gameContainer.getEventManager().callEvent(event);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        keys[keyCode] = false;
        KeyUpEvent event = new KeyUpEvent(keyCode);
        gameContainer.getEventManager().callEvent(event);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        buttons[mouseEvent.getButton()] = true;
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        buttons[mouseEvent.getButton()] = false;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        mouseX = (int) (mouseEvent.getX() / SCALE);
        mouseY = (int) (mouseEvent.getY() / SCALE);
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        mouseX = (int) (mouseEvent.getX() / SCALE);
        mouseY = (int) (mouseEvent.getY() / SCALE);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
        scroll = mouseWheelEvent.getWheelRotation();
    }
}
