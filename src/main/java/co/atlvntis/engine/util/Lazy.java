package co.atlvntis.engine.util;

import java.util.function.Supplier;

public class Lazy<T> {

    private volatile T value;

    public T getValue() {
        if(value == null) throw new NullPointerException("Value not computed yet.");
        return value;
    }

    public void compute(Supplier<T> supplier) {
        this.value = supplier.get();
    }

}
